﻿#include <iostream>

const int N = 25;

void printIntsToNByEven(int n, bool even) 
{
	for (int i = 0; i <= n; ++i) {
		if (even == (i % 2 == 0)) {
			std::cout << i << std::endl;
		}
	}
}

int main()
{
	for (int i = 0; i <= N; ++i) 
	{
		if (i % 2 == 0)
		{
			std::cout << i << std::endl;
		}
	}

	std::cout << std::endl;
	printIntsToNByEven(N, true);

	std::cout << std::endl;
	printIntsToNByEven(N, false);
}
